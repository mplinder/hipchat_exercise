# Message Parser #

This is a coding exercise designed to parse mentions, emoticons, and links from a given message string. It has been tested with Python 2.7.5 and Python 3.4.2. Here is the prompt it is designed to answer:

```text
Please write, in Python, code that takes a chat message string and returns a JSON string containing information about its contents. Special content to look for includes:
1. @mentions - A way to mention a user. Always starts with an '@' and ends when hitting a non-word character. (http://help.hipchat.com/knowledgebase/articles/64429-how-do-mentions-work-)
2. Emoticons - For this exercise, you only need to consider 'custom' emoticons which are ASCII strings, no longer than 15 characters, contained in parenthesis. You can assume that anything matching this format is an emoticon. (http://hipchat-emoticons.nyh.name)
3. Links - Any URLs contained in the message, along with the page's title.
 
For example, calling your function with the following inputs should result in the corresponding return values.
Input: "@chris you around?"
Return (string):
{
  "mentions": [
    "chris"
  ]
}

Input: "Good morning! (megusta) (coffee)"
Return (string):
{
  "emoticons": [
    "megusta",
    "coffee"
  ]
}


Input: "Olympics are starting soon; http://www.nbcolympics.com"
Return (string):
{
  "links": [
    {
      "url": "http://www.nbcolympics.com",
      "title": "NBC Olympics | 2014 NBC Olympics in Sochi Russia"
    }
  ]
}


Input: "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"
Return (string):
{
  "mentions": [
    "bob",
    "john"
  ],
  "emoticons": [
    "success"
  ],
  "links": [
    {
      "url": "https://twitter.com/jdorfman/status/430511497475670016",
      "title": "Twitter / jdorfman: nice @littlebigdetail from ..."
    }
  ]
}
```

## Notes on Requirements ##

### URLs ###
* URLs must have a valid scheme. Valid schemes are 'http' and 'https'.
* If for any reason `message_parser` is unable to make a connection to the specified URL, no link will be returned for the URL.
* Redirects are followed when accessing pages at the given URL.
* If the HTML returned from the given URL does not have a `<title>` tag, the `title` will be omitted from the JSON response, but the `url` will still be present.
* If the HTML returned from the given URL does have a `<title>`, it will be returned, even if the URL returned a 404 response code.
* Titles will be truncated to 50 characters including an ellipsis if the title is more than 50 characters.

## Setup ##
I recommend using `pip` and `virtualenv`, but whatever you choose to use, dependencies are outlined in `requirements.txt`.

Example:
```
$ virtualenv .
$ source bin/activate
$ git clone ...
$ cd hipchat_exercise
$ pip install -r requirements.txt
```

## Usage ##
Inside the `hipchat_exercise` directory:

```
>>> from message_parser import parse_message
>>> parse_message("@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016")
'{"mentions": ["bob", "john"], "emoticons": ["success"], "links": [{"url": "https://twitter.com/jdorfman/status/430511497475670016", "title": "Justin Dorfman on Twitter: &quot;nice @littlebi..."}]}'
```

## Logging ##
This project writes log files to `message_parser.log` in the same directory in which it is run. Logs are rotated when they reach 100kb, and a maximum of 5 log files are kept at any given time. 

## Running Tests ##
This project uses pytest and includes both unit and functional tests. Functional tests require network access to run properly.

Inside the `hipchat_exercise` directory, run tests with:
```
$ py.test
```

Optionally, see a coverage report with:
```
$ py.test --cov .
```

## PEP8 compliance ##
This project attempts to be PEP8 compliant. Check PEP8 compliance with:
```
$ pep8 message_parser.py tests/test_unit.py tests/test_functional.py
```