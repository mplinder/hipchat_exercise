import json
import message_parser
import requests


class TestMessageParser(object):

    def _get_title(self, url):
        resp = requests.get(url)

        title_start = resp.text.index('<title>') + len('<title>')
        title_end = resp.text.index('</title>')

        title = resp.text[title_start:title_end]

        return title[:47] + '...' if len(title) > 50 else title

    def test_parse_message_mentions_only(self):
        message = '@chris you around?'

        expected = {
            'mentions': ['chris'],
        }

        response = message_parser.parse_message(message)

        assert json.loads(response) == expected

    def test_parse_message_emoticsons_only(self):
        message = 'Good morning! (megusta) (coffee)'

        expected = {
            'emoticons': ['megusta', 'coffee'],
        }

        response = message_parser.parse_message(message)

        assert json.loads(response) == expected

    def test_parse_message_urls_only(self):
        url = 'http://www.nbcolympics.com'
        message = 'Olympics are starting soon; http://www.nbcolympics.com'

        title = self._get_title(url)

        expected = {
            'links': [
                {
                    'url': url,
                    'title': title,
                }
            ]
        }

        response = message_parser.parse_message(message)

        assert json.loads(response) == expected

    def test_parse_message_full_message(self):
        url = 'https://twitter.com/jdorfman/status/430511497475670016'
        message = '@bob @john (success) such a cool feature; ' \
                  'https://twitter.com/jdorfman/status/430511497475670016'

        title = self._get_title(url)

        expected = {
            'mentions': ['bob', 'john'],
            'emoticons': ['success'],
            'links': [
                {
                    'url': url,
                    'title': title,
                }
            ]
        }

        response = message_parser.parse_message(message)

        assert json.loads(response) == expected
