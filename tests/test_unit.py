from imp import reload
from mock import Mock, patch

import json
import message_parser
import pytest


class TestMessageParser(object):

    full_message = '@bob @john (success) such a cool feature; ' \
                   'https://twitter.com/jdorfman/status/430511497475670016'

    @patch.object(message_parser, '_parse_emoticons')
    @patch.object(message_parser, '_parse_and_get_urls')
    @patch.object(message_parser, '_parse_mentions')
    def test_parse_message(self, mentions_mock, links_mock, emoticons_mock):
        mentions_mock.return_value = ['bob', 'john']

        url = 'https://twitter.com/jdorfman/status/430511497475670016'
        title = 'Twitter / jdorfman: nice @littlebigdetail from ...'
        links_mock.return_value = [
            {
                'url': url,
                'title': title,
            }
        ]

        emoticons_mock.return_value = ['success']

        expected = {
            'mentions': ['bob', 'john'],
            'emoticons': ['success'],
            'links': [
                {
                    'url': url,
                    'title': title,
                }
            ]
        }

        response = message_parser.parse_message(self.full_message)

        assert json.loads(response) == expected

    def test_parse_mentions_one_mention(self):
        message = '@chris'
        expected = ['chris']

        assert message_parser._parse_mentions(message) == expected

    def test_parse_mentions_extra_characters(self):
        message = '@chris[(.;'
        expected = ['chris']

        assert message_parser._parse_mentions(message) == expected

    def test_parse_mentions_multiple_mentions(self):
        message = '@chris@john'
        expected = ['chris', 'john']

        assert message_parser._parse_mentions(message) == expected

    def test_parse_mentions_multiple_mentions_extra_characters(self):
        message = '@chris[(.;@john'
        expected = ['chris', 'john']

        assert message_parser._parse_mentions(message) == expected

    def test_parse_mentions_no_mentions(self):
        message = ''
        expected = []

        assert message_parser._parse_mentions(message) == expected

    def test_parse_mentions_double_symbol(self):
        message = '@@chris'
        expected = ['chris']

        assert message_parser._parse_mentions(message) == expected

    def test_parse_mentions_substring(self):
        message = 'notamention@chris'
        expected = ['chris']

        assert message_parser._parse_mentions(message) == expected

    def test_parse_mentions_full_message(self):
        expected = ['bob', 'john']

        assert message_parser._parse_mentions(self.full_message) == expected

    @patch.object(message_parser, '_get_title')
    @patch.object(message_parser, '_get_possible_url')
    def test_parse_and_get_urls(self, mock_get_possible_url, mock_get_title):
        mock_get_possible_url.return_value = 'foo'
        mock_get_title.return_value = 'bar'

        message = 'url_here'

        expected = [
            {
                'url': 'foo',
                'title': 'bar',
            }
        ]

        assert message_parser._parse_and_get_urls(message) == expected
        assert mock_get_possible_url.call_count == 1
        assert mock_get_title.call_count == 1

    @patch.object(message_parser, '_get_title')
    @patch.object(message_parser, '_get_possible_url')
    def test_parse_and_get_urls_multiple_urls(self,
                                              mock_get_possible_url,
                                              mock_get_title):
        mock_get_possible_url.side_effect = ['foo', 'bar']
        mock_get_title.side_effect = ['bar', 'baz']

        message = 'url_here second_url_here'

        expected = [
            {
                'url': 'foo',
                'title': 'bar',
            },
            {
                'url': 'bar',
                'title': 'baz',
            }
        ]

        assert message_parser._parse_and_get_urls(message) == expected
        assert mock_get_possible_url.call_count == 2
        assert mock_get_title.call_count == 2

    @patch.object(message_parser, '_get_title')
    @patch.object(message_parser, '_get_possible_url')
    def test_parse_and_get_urls_empty_string(self,
                                             mock_get_possible_url,
                                             mock_get_title):
        message = ''
        expected = []

        assert message_parser._parse_and_get_urls(message) == expected
        assert mock_get_possible_url.call_count == 0
        assert mock_get_title.call_count == 0

    @patch.object(message_parser, '_get_title')
    @patch.object(message_parser, '_get_possible_url')
    def test_parse_and_get_urls_no_urls(self,
                                        mock_get_possible_url,
                                        mock_get_title):
        mock_get_possible_url.return_value = None
        message = 'url_here'
        expected = []

        assert message_parser._parse_and_get_urls(message) == expected
        assert mock_get_possible_url.call_count == 1
        assert mock_get_title.call_count == 0

    @patch.object(message_parser, '_get_title')
    @patch.object(message_parser, '_get_possible_url')
    def test_parse_and_get_urls_no_title(self,
                                         mock_get_possible_url,
                                         mock_get_title):
        mock_get_possible_url.return_value = 'foo'
        mock_get_title.return_value = None

        message = 'irrelevant'
        expected = [{'url': 'foo'}]

        assert message_parser._parse_and_get_urls(message) == expected
        assert mock_get_possible_url.call_count == 1
        assert mock_get_title.call_count == 1

    @patch.object(message_parser, '_get_title')
    @patch.object(message_parser, '_get_possible_url')
    def test_parse_and_get_urls_bad_url(self,
                                        mock_get_possible_url,
                                        mock_get_title):
        mock_get_possible_url.return_value = 'foo'
        mock_get_title.side_effect = ValueError

        message = 'irrelevant'
        expected = []

        assert message_parser._parse_and_get_urls(message) == expected
        assert mock_get_possible_url.call_count == 1
        assert mock_get_title.call_count == 1

    def test_parse_emoticons_simple(self):
        message = '(success)'
        expected = ['success']

        assert message_parser._parse_emoticons(message) == expected

    def test_parse_emoticons_two_emoticons(self):
        message = '(success)(success)'
        expected = ['success', 'success']

        assert message_parser._parse_emoticons(message) == expected

    def test_parse_emoticons_no_emoticons(self):
        message = ''
        expected = []

        assert message_parser._parse_emoticons(message) == expected

    def test_parse_emoticons_empty_parens(self):
        message = '()'
        expected = []

        assert message_parser._parse_emoticons(message) == expected

    def test_parse_emoticons_non_word_chars(self):
        message = '(suc[]cess)'
        expected = ['suc[]cess']

        assert message_parser._parse_emoticons(message) == expected

    def test_parse_emoticons_max_length(self):
        message = '(thiswordistoolong)'
        expected = []

        assert message_parser._parse_emoticons(message) == expected

    def test_parse_emoticons_full_message(self):
        expected = ['success']

        assert message_parser._parse_emoticons(self.full_message) == expected

    def test_get_possible_url(self):
        message = 'http://foo.com'
        expected = 'http://foo.com'

        assert message_parser._get_possible_url(message) == expected

    def test_get_possible_url_empty_string(self):
        message = ''
        expected = None

        assert message_parser._get_possible_url(message) == expected

    def test_get_possible_url_no_url(self):
        message = 'foo bar baz'
        expected = None

        assert message_parser._get_possible_url(message) == expected

    def test_get_possible_url_two_urls(self):
        message = 'http://foo.comhttp://bar.com'
        expected = 'http://foo.comhttp://bar.com'

        assert message_parser._get_possible_url(message) == expected

    def test_get_possible_url_https(self):
        message = 'https://foo.com'
        expected = 'https://foo.com'

        assert message_parser._get_possible_url(message) == expected

    def test_get_possible_url_no_scheme(self):
        message = 'foo.com'
        expected = None

        assert message_parser._get_possible_url(message) == expected

    def test_get_possible_url_no_netloc(self):
        message = 'http://'
        expected = None

        assert message_parser._get_possible_url(message) == expected

    def test_get_possible_url_leading_characters(self):
        message = 'thesecharsaremeaninglesshttp://foo.com'
        expected = 'http://foo.com'

        assert message_parser._get_possible_url(message) == expected

    @patch.object(message_parser, 'requests')
    def test_get_title(self, mock_requests):
        mock_response = Mock()
        mock_response.text = '<title>test title</title>'
        mock_requests.get.return_value = mock_response
        expected = 'test title'

        assert message_parser._get_title('irrelevant') == expected

    @patch.object(message_parser, 'requests')
    def test_get_title_max_length(self, mock_requests):
        mock_response = Mock()
        mock_response.text = '<title>this title is totally more ' \
                             'than 50 characters so truncate it</title>'
        mock_requests.get.return_value = mock_response
        expected = 'this title is totally more ' \
                   'than 50 characters s...'

        assert message_parser._get_title('irrelevant') == expected

    @patch.object(message_parser, 'requests')
    def test_get_title_max_length_no_ellipsis(self, mock_requests):
        mock_response = Mock()
        mock_response.text = '<title>this title is exactly 50 chars ' \
                             'so no ellipsis here</title>'
        mock_requests.get.return_value = mock_response
        expected = 'this title is exactly 50 chars so no ellipsis here'

        assert message_parser._get_title('irrelevant') == expected

    @patch.object(message_parser, 'requests')
    def test_get_title_request_error(self, mock_requests):
        mock_requests.exceptions.RequestException = Exception
        mock_requests.get.side_effect = \
            mock_requests.exceptions.RequestException
        mock_response = Mock()
        mock_requests.get.return_value = mock_response

        with pytest.raises(ValueError):
            response = message_parser._get_title('irrelevant')

    @patch.object(message_parser, 'requests')
    def test_get_title_no_title(self, mock_requests):
        mock_response = Mock()
        mock_response.text = ''
        mock_requests.get.return_value = mock_response
        expected = None

        assert message_parser._get_title('irrelevant') == expected
        assert mock_requests.get.call_count == 1

    @patch.object(message_parser, 'requests')
    def test_get_title_no_title_start(self, mock_requests):
        mock_response = Mock()
        mock_response.text = 'test title</title>'
        mock_requests.get.return_value = mock_response
        expected = None

        assert message_parser._get_title('irrelevant') == expected
        assert mock_requests.get.call_count == 1

    @patch.object(message_parser, 'requests')
    def test_get_title_no_title_end(self, mock_requests):
        mock_response = Mock()
        mock_response.text = '<title>test title'
        mock_requests.get.return_value = mock_response
        expected = None

        assert message_parser._get_title('irrelevant') == expected
        assert mock_requests.get.call_count == 1

    def test_compatibility(self):
        """
        This test is required for 100% coverage under Python 2.7.
        """
        mock_urllib = Mock()
        with patch.dict('sys.modules', {'urlparse': {}}):
            with patch.dict('sys.modules', {'urllib.parse': mock_urllib}):
                reload(message_parser)
                assert message_parser.urlparse == mock_urllib.urlparse
