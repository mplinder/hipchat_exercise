import json
import logging
import logging.handlers
import re
import requests

# For Python 2 and 3 compatibility
try:
    from urlparse import urlparse
except ImportError:
    from urllib.parse import urlparse


LOG_FILENAME = 'message_parser.log'

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

if len(logger.handlers) == 0:
    handler = logging.handlers.RotatingFileHandler(
        LOG_FILENAME,
        maxBytes=100000,
        backupCount=5
    )

    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    )

    handler.setFormatter(formatter)

    logger.addHandler(handler)


EMOTICON_START = '('
EMOTICON_END = ')'
EMOTICON_MAX_LENGTH = 15
HTML_TITLE_OPEN = '<title>'
HTML_TITLE_CLOSE = '</title>'
TITLE_MAX_LENGTH = 50
HTTP_SCHEME = 'http'
MENTION_SYMBOL = '@'


def parse_message(message):
    """
    Takes in a string, parses mentions, links, and emoticons and returns
    a JSON formatted string. Mentions, links, and emoticons will each
    only be present in the returned JSON string if they are present in
    the passed message.
    :param message: string
    :return: JSON string in the format:
        '{
            "mentions": [<string>,],
            "emoticons": [<string>,],
            "links": [
                {
                    "url": <string>,
                    "title": <string>
                }
            ]
        }'
    """
    logger.debug('Message received: {}'.format(message))

    mentions = _parse_mentions(message)
    links = _parse_and_get_urls(message)
    emoticons = _parse_emoticons(message)

    response = {}

    if mentions:
        response['mentions'] = mentions

    if links:
        response['links'] = links

    if emoticons:
        response['emoticons'] = emoticons

    response = json.dumps(response)

    logger.debug('Returning response: {}'.format(response))

    return response


def _parse_mentions(message):
    """
    Find mentions in a string and return them in a list. Mentions must
    start with MENTION_SYMBOL and end at a non-word character. Returned
    mentions will have the MENTION_SYMBOL character removed.
    :param message: string
    :return: list of mentions
    """
    mentions = []
    if MENTION_SYMBOL in message:
        candidates = message.split(MENTION_SYMBOL)[1:]
    else:
        candidates = []

    for candidate in candidates:
        mention = re.compile('\W').split(candidate)[0]
        if mention:
            mentions.append(mention)

    return mentions


def _parse_and_get_urls(message):
    """
    Find URLs in a string and get their title. Attempts to verify URL
    before incurring network access costs. If no title is found for
    the URL, the title attribute will not be included in the response.
    :param message: string
    :return: list of dicts in the format:
        [
            {
                'url': <string>,
                'title': <string>,
            },
        ]
    """
    links = []
    for word in message.split():
        url = _get_possible_url(word)
        if url is None:
            continue

        try:
            title = _get_title(url)
        except ValueError:
            continue

        link = {
            'url': url,
        }

        if title is not None:
            link['title'] = title

        links.append(link)

    return links


def _get_possible_url(word):
    """
    Find a URL in the given string. Valid URLs must start with
    HTTP_SCHEME, but beyond that, a URL can be just about anything,
    including another URL as a query parameter. See
    https://tools.ietf.org/html/rfc3986 for more details on
    valid URLs. As such, this method will only return one URL from
    the given word.
    :param word: string
    :return: substring of the input string which contains a URL or None
    """
    try:
        url = word[word.index(HTTP_SCHEME):]
    except ValueError:
        return None

    url_parts = urlparse(url)
    if not all([url_parts.scheme, url_parts.netloc]):
        return None

    return url


def _get_title(url):
    """
    Get the title of the page served from the given URL. Returns None if
    the returned HTML does not have an HTML_TITLE_OPEN or
    HTML_TITLE_CLOSE attribute. Truncates title to TITLE_MAX_LENGTH
    characters (including ellipsis if necessary).
    :param url: URL to make a request to
    :return: string, title of the page served by the given URL
    :raises ValueError: raises ValueError if url is invalid
    """
    try:
        resp = requests.get(url)
    except requests.exceptions.RequestException:
        message = 'Unable to load page served from {}'.format(url)
        logger.info(message, exc_info=True)

        raise ValueError(message)

    # If we were doing anything more complex than getting the title here,
    # I would use lxml, but it's not worth the extra dependency for what
    # is ultimately just a string match.
    try:
        title_start = resp.text.index(HTML_TITLE_OPEN) + len(HTML_TITLE_OPEN)
        title_end = resp.text.index(HTML_TITLE_CLOSE)
    except ValueError:
        return None

    title = resp.text[title_start:title_end]

    return title[:TITLE_MAX_LENGTH - 3] + '...' \
        if len(title) > TITLE_MAX_LENGTH else title


def _parse_emoticons(message):
    """
    Find emoticons in a string. Emoticons must start with EMOTICON_START
    and end with EMOTICON_END and have EMOTICON_MAX_LENGTH or fewer
    characters in between. Emoticons returned will have parentheses
    stripped.
    :param message: string
    :return: list of emoticons
    """
    emoticons = []
    candidates = message.split(EMOTICON_START)

    for candidate in candidates:
        try:
            emoticon = candidate[:candidate.index(EMOTICON_END)]
        except ValueError:
            continue

        if emoticon and len(emoticon) <= EMOTICON_MAX_LENGTH:
            emoticons.append(emoticon)

    return emoticons
